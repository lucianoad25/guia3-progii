package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */

public class ejercicio10 {
public static void isbn() {
	Scanner entrada = new Scanner(System.in);
	int  codigo[] = new int[9];
	int  codigoMult[] = new int[9];
	for (int i=0; i<codigo.length; i++) {
		System.out.println("Ingrese entero en la posici�n " + i);
		codigo[i] = entrada.nextInt(); 
	}
	int total = 0;
	for (int i=0; i<codigo.length; i++) {
		
		codigoMult[i] = codigo[i] * (i+1);					
		total = total + codigoMult[i];
	}
	
	int verificador = total % 11;
	
	System.out.print("ISBN ");
	for (int i=0; i<codigo.length; i++) {
		System.out.print(codigo[i]);
		if (i==0 || i==4 || i==8) {
			System.out.print("-");
		}
	}
	System.out.print(verificador);
	entrada.close();
}
}
