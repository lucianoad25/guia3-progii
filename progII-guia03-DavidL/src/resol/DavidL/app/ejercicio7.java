package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio7 {

	final static public void contadorpalabras() {
		int cantpalabras = 0;    
		   
		Scanner teclado = new Scanner(System.in);
		System.out.print("Ingrese frase : ");
	    String frase=teclado.nextLine();
	    teclado.close();
		   
		boolean palabra = false;
		int finLinea = frase.length() - 1;

		for (int i = 0; i < frase.length(); i++) {
			if (Character.isLetter(frase.charAt(i)) && i != finLinea) {
				palabra = true;
			} else if (!Character.isLetter(frase.charAt(i)) && palabra) {
				cantpalabras++;
		        palabra = false;
		    } else if (Character.isLetter(frase.charAt(i)) && i == finLinea) {
		    	cantpalabras++;
		    }
		}    
		
		System.out.print("Cantidad de palabras:" +cantpalabras);  
	}
}
