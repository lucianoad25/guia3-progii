package resol.DavidL.app;

import java.util.Scanner;
/**
 * @author Luciano David
 *
 */


public class ejercicio3 {		
		
		static public void capicua() {
			
			Scanner teclado = new Scanner(System.in);
			
					
			System.out.println("Ingrese numero : ");					
			int numero = teclado.nextInt();
			teclado.close();
			
			int falta=numero;
			int invertido=0;
			int resto=0;
			  		 	  
			  while(falta!=0) {
			       
				    resto=falta%10;
			        invertido=invertido*10+resto;
			        falta=falta/10;
					}
			 
			  if(invertido==numero){
					 System.out.println("El numero "+numero+" es capicua");
			        }
			  else{
			        System.out.println("El numero "+numero+" no es capicua");
			  		}
			}
	}
	
	

