package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio9 {
	public static void ordenados() {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Ingrese tama�o del arreglo: ");
		int dimension = entrada.nextInt();
		int  enterosOrd[] = new int[dimension];
		
		for (int i=0; i<enterosOrd.length; i++) {
			System.out.println("Ingrese entero en la posici�n " + i);
			enterosOrd[i] = entrada.nextInt();
		}

		int temporal;
		for (int i = 0; i < enterosOrd.length; i++) {
			
	        for (int j = 1; j < (enterosOrd.length - i); j++) {
	            if (enterosOrd[j - 1] > enterosOrd[j]) {
	                temporal = enterosOrd[j - 1];
	                enterosOrd[j - 1] = enterosOrd[j];
	                enterosOrd[j] = temporal;
	            }
	        }
		}
		
		System.out.println("Arreglo ordenado");
		for (int i=0; i<enterosOrd.length; i++) {
			System.out.print("|"+enterosOrd[i]+"| ");
			
		}
		
		
	
		entrada.close();
	}
}
