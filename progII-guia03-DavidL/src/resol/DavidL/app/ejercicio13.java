package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio13 {
	public static final void recurIter() {
		int aux=0;
		Scanner teclado = new Scanner(System.in);
		System.out.print("Ingrese Tama�o a sumar : ");
		int tama�o = teclado.nextInt();
		teclado.close();
			
		for (int i=1; i<tama�o+1; i++) {
			aux=aux+i;
		}

		System.out.println("Por recurisvidad "+recu(tama�o));
		System.out.println("Por iteracion "+aux);
	}
	
	
	public static int recu(int n) {
		/**
	     * M�todo recursivo
	     */
	
		if(n==1){    
		      return n;}
		else {
		return n+recu(n-1);}
	}
}
