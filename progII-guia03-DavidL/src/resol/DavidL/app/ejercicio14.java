package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */

public class ejercicio14 {

	public static int[] llenar(int x[]) {
		/**
		 * M�todo para cargar el array.
		 */
		Scanner entrada = new Scanner(System.in);
		System.out.print(x.length);
		for (int i=0; i<x.length; i++) {
			System.out.print("Ingrese el valor en la posici�n " + i + ": ");
			x[i] = entrada.nextInt();
		}
		entrada.close();
		return x;
	}
	
	
	public static int[] mostrar(int x[]) {
		/**
	     * M�todo para leer el vector
	     */
		Scanner entrada = new Scanner(System.in);
		for (int i=0; i<x.length; i++) {
			System.out.println("El valor en la posici�n " + i + " es: " + x[i]);	
		}
		entrada.close();
		return x;
	}
	public static final void cargarMostrar() {
		Scanner entrada = new Scanner(System.in);
		int  numeros[] = new int[10];
		llenar(numeros);
		mostrar(numeros);
		entrada.close();
	}
}
