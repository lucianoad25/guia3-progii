package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */

public class ejercicio16 {
	
	public static void calificaciones() {
		
		Scanner teclado = new Scanner(System.in);
		
		float notas[] = new float[3];
		String nombres[] = new String[3];
		String notasPal[] = new String[3];
		
		for (int i=0; i<notas.length; i++) {
			System.out.print("Ingrese alumno: ");
			nombres[i] = teclado.next();
			System.out.print("Ingrese nota: ");
			notas[i] = teclado.nextInt();
			if (notas[i] > 0 && notas[i] < 5) {
				notasPal[i] = "Suspenso";
			} else if (notas[i] < 7) {
				notasPal[i] = "Bien";
			} else if (notas[i] < 9) {
				notasPal[i] = "Notable";
			} else if (notas[i] <=10){
				notasPal[i] = "Sobresaliente";
			}
		}
	
		teclado.close();
		
		for (int i=0; i<notas.length; i++) {
			System.out.println("Alumno: " + nombres[i] + " - Nota: " + notas[i] + " es un " + notasPal[i]);
		}
	}

}
