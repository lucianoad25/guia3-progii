package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio12 {
	public static final void ternario() {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Ingrese el valor A: ");
		int A = entrada.nextInt();
		System.out.println((A % 2 == 0) ? A + " es par" : A + " es impar") ;
		
		
		System.out.print("Ingrese el valor C: ");
		int C = entrada.nextInt();
		System.out.println((C >= 0) ? C + " es positivo" : C + " es negativo");
		System.out.println((C % 2 == 0) ? C + " es par" : C + " es impar");
		System.out.println((C % 5 == 0) ? C + " es m�ltiplo de 5" : C + " no es m�ltiplo de 5");
		System.out.println((C % 10 == 0) ? C + " es m�ltiplo de 10" : C + " no es m�ltiplo de 10");
		System.out.println((C > 100) ? C + " es mayor que 100" : C + " no es mayor que 100") ;
	    
		entrada.close();
	}
}
