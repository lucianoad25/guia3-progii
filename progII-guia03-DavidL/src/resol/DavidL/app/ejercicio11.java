package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */

public class ejercicio11 {

	public static final void operaciones() {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Ingrese el valor N: ");
		int N = entrada.nextInt();
		System.out.print("Ingrese el valor A: ");
		double A = entrada.nextDouble();
		System.out.print("Ingrese el valor C: ");
		char C = entrada.next().charAt(0);
		entrada.close();
		
		System.out.println("El valor de N es: " + N);
		System.out.println("El valor de A es: " + A);
		System.out.println("El valor de C es: " + C);
		
		double suma = N + A;
		System.out.println("La suma de " + N + " + " + A + " es: " + suma);
		
		double diferencia = A - N;
		System.out.println("La diferencia de " + A + " - " + N + " es: " + diferencia);
		
		int valor = (int) C;
		System.out.println("Valor num�rico correspondiente al car�cter " + C + " es: " + valor);
		
		double total = suma + valor;
		System.out.println("La suma total es: " + total);
	}
}
