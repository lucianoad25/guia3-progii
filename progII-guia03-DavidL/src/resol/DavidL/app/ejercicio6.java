package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio6 {
	public static final void primo() {
		System.out.print("Ingrese un n�mero para ver si es primo: ");
		Scanner entrada = new Scanner(System.in);
		int numero = entrada.nextInt();
		entrada.close();
		int i=2;
		boolean primo = true;
		
		for (i=2; (i<(numero/2) && primo==true); i++) {
			if (numero % i == 0) {
				primo = false;
			}
		}
		
		if (primo) {
			System.out.println("El n�mero ingresado es primo.");
		} else {
			System.out.println("El n�mero ingresado no es primo ya que es divisible por " + (i-1));			
		}
		
		
	}
}
