/**
 * 
 */
package resol.DavidL.app;

import java.util.Scanner;

/**
 * @author Luciano David
 *
 * Clase que tiene el main y llama a las demas clases
 *
 */
public class Principal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Ingrese el numeor de ejercicio que quiere ejecutar:  ");
		Scanner teclado = new Scanner(System.in);		
		// TODO Auto-generated method stub		
		
		int ejercicio = teclado.nextInt();
		
		
		switch(ejercicio) {
		case 1:
			ejercicio1.saludo();
			break;
		case 2:
			ejercicio2.promedio();
			break;
		case 3:
			ejercicio3.capicua();
			break;
		case 4:
			ejercicio4.password();
			break;
		case 5:
			ejercicio5.fibonacci();
			break;
		case 6:
			ejercicio6.primo();;
			break;
		case 7:
			ejercicio7.contadorpalabras();;
			break;
		case 8:
			ejercicio8.billetes();
			break;
		case 9:
			ejercicio9.ordenados();
			break;
		case 10:
			ejercicio10.isbn();
			break;
		case 11:
			ejercicio11.operaciones();
			break;
		case 12:
			ejercicio12.ternario();
			break;
		case 13:
			ejercicio13.recurIter();
			break;
		case 14:
			ejercicio14.cargarMostrar();
			break;
		case 15:
			ejercicio15.Vocales();
				break;	
		case 16:
			ejercicio16.calificaciones();
			break;
			
			
		}
		teclado.close();
	}

}
