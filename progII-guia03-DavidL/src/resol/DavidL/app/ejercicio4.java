package resol.DavidL.app;
import java.util.Scanner;

/**
 * @author Luciano David
 *
 */
public class ejercicio4 {
	public static void password() {
		System.out.print("Ingrese una contraseņa para ver si es segura: ");
		Scanner entrada = new Scanner(System.in);
		String pass = entrada.next();
		entrada.close();
		int cantCaract = pass.length();
		int may = 0;
		int min = 0;
		boolean segura = true;
		
		if (cantCaract<8) {
			segura = false;
		} else {
			for (int i=0; i<cantCaract; i++) {
				if (pass.codePointAt(i) < 33 && pass.codePointAt(i) > 126) {
					segura = false;
					break;
				} else if (pass.codePointAt(i) > 64 && pass.codePointAt(i) < 91) {
					may++;
				} else if (pass.codePointAt(i) > 96 && pass.codePointAt(i) < 123) {
					min++;
				}
			}
		}
		
		if (segura && min >= 3 && may >=2) {
			System.out.println("La contraseņa ingresada es segura.");
		} else {
			System.out.println("La contraseņa ingresada es insegura o posee caracteres incorrectos.");			
		}
	}

	
	
	
}
