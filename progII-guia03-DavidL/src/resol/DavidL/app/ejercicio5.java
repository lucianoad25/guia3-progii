package resol.DavidL.app;

import java.util.Scanner;
/**
 * @author Luciano David
 *
 */

public class ejercicio5 {
	
	public static void fibonacci() {
		System.out.print("Ingrese la cantidad de numeros de la sucesi�n de Fibonacci que desea ver: ");
		Scanner entrada = new Scanner(System.in);
		int n = entrada.nextInt();
		
		for (int i=0; i<n; i++) {
			
			System.out.println(fibonacci_calc(i));
		}
		entrada.close();
	}

	public static int fibonacci_calc(int n) {
	    if (n>1){
	       return fibonacci_calc(n-1) + fibonacci_calc(n-2);  
	    }
	    else if (n==1) {  // caso base
	        return 1;
	    }
	    else if (n==0){  // caso base
	        return 0;
	    }
	    else{ //error
	        System.out.println("Debes ingresar un tama�o mayor o igual a 1");
	        return -1; 
	    }
	}
	
}
